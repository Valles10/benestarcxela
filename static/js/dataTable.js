let $table = $('#data-table');
let urlEditar, urlEliminar, urlDetalles  = '';

function btnEdit(data, linkEdit){
    console.log(data);

}
function simpleBtns(data, linkEdit) {
    urlEditar = `${urlEditar.replace("0", data)}`;
    urlEliminar = `${urlEliminar.replace("0", data)}`;
    const btnEditar = (linkEdit) ? `<a class="edit-button btn btn-info btn-fab btn-icon btn-round btn-sm" title="Actualizar" href="${urlEditar}"><i class="tim-icons icon-pencil"></i></a>`
        : `<button class="edit-button btn btn-info btn-fab btn-icon btn-round btn-sm" title="Actualizar" data-url="${urlEditar}"><i class="tim-icons icon-pencil"></i></button>`;
    const btnEliminar = `<button class="delete-button btn btn-danger btn-fab btn-icon btn-round btn-sm" title="Eliminar" data-url="${urlEliminar}"><i class="tim-icons icon-trash-simple"></i></button>`;
    urlEditar = `${urlEditar.replace(data, "0")}`;
    urlEliminar = `${urlEliminar.replace(data, "0")}`;
    return `<div class="text-center">${btnEditar}${btnEliminar}</div>`;
}

function detailsBtns(data) {
    urlDetalles = `${urlDetalles.replace("0", data)}`;
    const btnDetalles = `<button class="details-button btn btn-success btn-fab btn-icon btn-round btn-sm" title="Detalles" data-url="${urlDetalles}"><i class="tim-icons icon-alert-circle-exc"></i></button>`;
    return `<div class="">${btnDetalles}</div>`;
}

function getDefaultDataTables(url, language_url, columns) {
    return {
        ajax: {
            url: url,
            type: 'post',
            headers: {'X-CSRFToken': getCookie('csrftoken')},
        },
        language: {
            url: language_url
        },
        lengthMenu: [[10, 25, 50], [10, 25, 50]],
        searching: true,
        sDom: "<<'col-12 mt-2 pull-left'f>tr><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columns: columns
    };
}


// region Eventos Botones DataTable
$table.on('click', '.delete-button', function () {
    const url = $(this).data('url');
    deleteDataTableItem(url, $table);
});

$table.on('click', '.edit-button', function () {
    const url = $(this).data('url');
    $('#form-modal-content').load(url, function () {
        $('#form-modal').modal('show');
        formAjaxSubmit('#form-modal-content form', '#form-modal', true);
    });
});

$table.on('click', '.details-button', function () {
    const url = $(this).data('url');
    $('#form-modal-content').load(url, function () {
        $('#form-modal').modal('show');
    });
});

// endregion
