
from catalogos.models import TipoPersonal
from utils.FormMixim import FieldSetModelFormMixin


class TipoPersonalForm(FieldSetModelFormMixin):
    class Meta:
        model = TipoPersonal

        fields = [
            'id', 'nombre'
        ]
