from django.http import JsonResponse

# Create your views here.
from django.views.generic import TemplateView, CreateView, UpdateView
from django_datatables_view.base_datatable_view import BaseDatatableView

from catalogos.forms import TipoPersonalForm
from catalogos.models import TipoPersonal
from utils.ResponseMixim import CreateFormResponseMixin, UpdateFormResponseMixin, JsonDeleteView


class TiposPersonalListView(TemplateView):
    template_name = 'tipos_personal/list.html'


class TiposPersonalListJson(BaseDatatableView):
    model = TipoPersonal
    columns = ['id', 'nombre']
    order_columns = ['id', 'nombre']


class TipoPersonalCreateView(CreateView, CreateFormResponseMixin):
    template_name = 'tipos_personal/create.html'
    model = TipoPersonal
    form_class = TipoPersonalForm


class TipoPersonalUpdateView(UpdateView, UpdateFormResponseMixin):
    template_name = 'tipos_personal/update.html'
    model = TipoPersonal
    form_class = TipoPersonalForm


class TipoPersonalDeleteJsonView(JsonDeleteView):
    model = TipoPersonal

    def post(self, request, *args, **kwargs):
        super().post(request, args, kwargs)
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})
