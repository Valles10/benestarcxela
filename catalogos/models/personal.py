from django.db import models


class TipoPersonal(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')

    class Meta:
        app_label = 'catalogos'
        verbose_name = 'tipo_personal'
        verbose_name_plural = 'tipos_de_personal'

    def __str__(self):
        return self.nombre


class Personal(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')

    # Relaciones
    tipo_personal = models.ForeignKey(TipoPersonal, verbose_name='Tipo de Personal', related_name='personal_tipo',
                                      on_delete=models.PROTECT)

    class Meta:
        app_label = 'catalogos'
        verbose_name = 'personal'
        verbose_name_plural = 'personal'

    def __str__(self):
        return self.nombre
