from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.db.models import QuerySet
from django.forms import formset_factory

from django.http import JsonResponse, HttpResponseRedirect

from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, DetailView
from django_datatables_view.base_datatable_view import BaseDatatableView

from clinica.forms import PacienteForm, DoctorForm, CitaForm, Cita, CategoriaForm, PesoForm, \
    MedidaForm, Diagnostico, DiagnosticoForm, QuiropracticaForm, FisioterapiaForm, Peso, Medida, Quiropractica, \
    Fisioterapia
from clinica.models import Paciente, Doctor, Categoria, Historial
from utils.PermissionsMixim import GroupRequiredMixin
from utils.ResponseMixim import CreateFormResponseMixin, UpdateFormResponseMixin, JsonDeleteView


class PacientesListView(TemplateView):
    template_name = 'pascientes/list.html'


class PacientesListJson(BaseDatatableView):
    model = Paciente
    columns = ['id', 'nombre', 'apellido', 'direccion', 'estado']
    order_columns = ['id', 'nombre', 'apellido', 'direccion', 'estado']


class PacienteCreateView(CreateView, CreateFormResponseMixin):
    template_name = 'pascientes/create.html'
    model = Paciente
    form_class = PacienteForm


class PacienteUpdateView(UpdateView, UpdateFormResponseMixin):
    template_name = 'pascientes/update.html'
    model = Paciente
    form_class = PacienteForm


class PacienteDeleteJsonView(JsonDeleteView, GroupRequiredMixin):
    group_required = ['Administrador']
    model = Paciente

    def post(self, request, *args, **kwargs):
        super().post(request, args, kwargs)
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})


class PacienteInactivoJsonView(JsonDeleteView, GroupRequiredMixin):
    group_required = ['Administrador']
    model = Paciente

    def post(self, request, *args, **kwargs):
        paciente = Paciente.objects.get(pk=kwargs.get('pk'))
        if paciente.estado is 1:
            paciente.estado = 2
        else:
            paciente.estado = 1
        paciente.save()
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})


class PacienteDetallesView(DetailView):
    model = Paciente
    template_name = 'pascientes/details.html'


class DoctoresListView(TemplateView):
    template_name = 'doctores/list.html'


class DoctoresListJson(BaseDatatableView):
    model = Doctor
    columns = ['id', 'nombre', 'apellido', 'telefono', 'estado']
    order_columns = ['id', 'nombre', 'apellido', 'telefono', 'estado']


class DoctorCreateView(CreateView, CreateFormResponseMixin):
    template_name = 'doctores/create.html'
    model = Doctor
    form_class = DoctorForm


class DoctorUpdateView(UpdateView, UpdateFormResponseMixin):
    template_name = 'doctores/update.html'
    model = Doctor
    form_class = DoctorForm


class DoctorDeleteJsonView(JsonDeleteView):
    model = Doctor

    def post(self, request, *args, **kwargs):
        super().post(request, args, kwargs)
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})


class DoctorEstadoJsonView(JsonDeleteView, GroupRequiredMixin):
    group_required = ['Administrador']
    model = Doctor

    def post(self, request, *args, **kwargs):
        doctor = Doctor.objects.get(pk=kwargs.get('pk'))
        if doctor.estado is 1:
            doctor.estado = 2
        else:
            doctor.estado = 1
        doctor.save()
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})


class DoctorDetallesView(DetailView):
    model = Doctor
    template_name = 'doctores/details.html'


class CategoriasListView(TemplateView):
    template_name = 'categorias/list.html'


class CategoriasListJson(BaseDatatableView):
    model = Categoria
    columns = ['id', 'nombre']
    order_columns = ['id', 'nombre']


class CategoriaCreateView(CreateView, CreateFormResponseMixin):
    template_name = 'categorias/create.html'
    model = Categoria
    form_class = CategoriaForm


class CategoriaUpdateView(UpdateView, UpdateFormResponseMixin):
    template_name = 'categorias/update.html'
    model = Categoria
    form_class = CategoriaForm


class CategoriaDeleteJsonView(JsonDeleteView):
    model = Categoria

    def post(self, request, *args, **kwargs):
        super().post(request, args, kwargs)
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})


class CitasListView(TemplateView):
    template_name = 'citas/list.html'


class CitasListJson(BaseDatatableView):
    model = None
    columns = ['id', 'fecha', 'hora', 'categoria.nombre', 'paciente', 'doctor']
    order_columns = ['id', 'fecha', 'hora', 'categoria.nombre', 'paciente', 'doctor']

    def get_initial_queryset(self):
        return Cita.objects.all().order_by('-fecha').order_by('-hora__hours')


class CitaCreateView(CreateView, CreateFormResponseMixin):
    template_name = 'citas/create.html'
    model = Cita
    form_class = CitaForm


class CitaUpdateView(UpdateView, UpdateFormResponseMixin):
    template_name = 'citas/update.html'
    model = Cita
    form_class = CitaForm


class CitaDeleteJsonView(JsonDeleteView):
    model = Cita

    def post(self, request, *args, **kwargs):
        super().post(request, args, kwargs)
        return JsonResponse({'result': 'OK', 'id': kwargs.get('pk')})


class DiagnosticoNewView(CreateView, CreateFormResponseMixin):
    template_name = 'diagnosticos/create.html'
    model = Diagnostico
    form_class = DiagnosticoForm
    success_url = reverse_lazy('clinica:historial')

    def get_form(self, form_class=None):
        form = super(DiagnosticoNewView, self).get_form(form_class)
        cita = Cita.objects.get(pk=self.kwargs.get('cita_id'))
        form.initial = {
            'fecha': cita.fecha
        }
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_quiropractica = extra_fisioterapia = extra_peso_medida = 0

        cita = Cita.objects.get(pk=self.kwargs.get('cita_id'))
        context['diagnostico'] = Diagnostico.objects.filter(cita_id=cita.id).count() > 0
        context['cita'] = cita
        context['paciente'] = cita.paciente
        if cita.categoria.nombre.lower() == 'quiropractica':
            extra_quiropractica = 1
        if cita.categoria.nombre.lower() == 'fisioterapia':
            extra_fisioterapia = 1
        if cita.categoria.nombre.lower() == 'peso y medida':
            extra_peso_medida = 1

        quiropractica_formset = formset_factory(QuiropracticaForm, extra=extra_quiropractica)
        context['formsetQuiropractica'] = quiropractica_formset(prefix='Quiropractica')

        fisioterapia_formset = formset_factory(FisioterapiaForm, extra=extra_fisioterapia)
        context['formsetFisioterapia'] = fisioterapia_formset(prefix='Fisioterapia')

        peso_formset = formset_factory(PesoForm, extra=extra_peso_medida)
        context['formsetPeso'] = peso_formset(prefix='Peso')
        medida_formset = formset_factory(MedidaForm, extra=extra_peso_medida)
        context['formsetMedida'] = medida_formset(prefix='Medida')
        return context

    def form_valid(self, form):
        extra_quiropractica = extra_fisioterapia = extra_peso_medida = 0
        cita = Cita.objects.get(pk=self.kwargs.get('cita_id'))
        if cita.categoria.nombre.lower() == 'quiropractica':
            extra_quiropractica = 1
        if cita.categoria.nombre.lower() == 'fisioterapia':
            extra_fisioterapia = 1
        if cita.categoria.nombre.lower() == 'peso y medida':
            extra_peso_medida = 1

        self.request.POST._mutable = True
        self.request.POST['Quiropractica-TOTAL_FORMS'] = extra_quiropractica
        self.request.POST['Fisioterapia-TOTAL_FORMS'] = extra_fisioterapia
        self.request.POST['Peso-TOTAL_FORMS'] = extra_peso_medida
        self.request.POST['Medida-TOTAL_FORMS'] = extra_peso_medida

        quiropractica_formset = formset_factory(QuiropracticaForm)
        formset_quiropractica = quiropractica_formset(self.request.POST, self.request.FILES, prefix='Quiropractica')

        fisioterapia_formset = formset_factory(FisioterapiaForm)
        formset_fisioterapia = fisioterapia_formset(self.request.POST, self.request.FILES, prefix='Fisioterapia')

        peso_formset = formset_factory(PesoForm)
        formset_peso = peso_formset(self.request.POST, self.request.FILES, prefix='Peso')
        medida_formset = formset_factory(MedidaForm)
        formset_medida = medida_formset(self.request.POST, self.request.FILES, prefix='Medida')

        with transaction.atomic():
            diagnostico = form.save(commit=False)
            diagnostico.usuario_id = self.request.user.pk
            diagnostico.cita_id = self.kwargs.get('cita_id')
            diagnostico.save()

            if formset_quiropractica.is_valid():
                for item in formset_quiropractica:
                    detalle = item.save(commit=False)
                    if detalle.precio_q is None:
                        detalle.precio_q = 0
                    detalle.diagnostico_id = diagnostico.pk
                    detalle.save()

            if formset_fisioterapia.is_valid():
                for item in formset_fisioterapia:
                    detalle = item.save(commit=False)
                    if detalle.precio_f is None:
                        detalle.precio_f = 1
                    detalle.diagnostico_id = diagnostico.pk
                    detalle.save()

            if formset_peso.is_valid():
                for item in formset_peso:
                    detalle = item.save(commit=False)
                    detalle.diagnostico_id = diagnostico.pk
                    detalle.save()

            if formset_medida.is_valid():
                for item in formset_medida:
                    detalle = item.save(commit=False)
                    detalle.diagnostico_id = diagnostico.pk
                    detalle.save()
            messages.success(self.request, 'Diagnostico guardado.')
            return JsonResponse({'result': 'OK', 'id': str(diagnostico.pk)})


class DiagnosticoDetailsView(DetailView):
    model = Paciente
    template_name = 'diagnosticos/details.html'

    def get_context_data(self, **kwargs):
        context = super(DiagnosticoDetailsView, self).get_context_data(**kwargs)
        historial = []
        citas = Cita.objects.filter(paciente=self.object)
        for cita in citas:
            diagnostico = Diagnostico.objects.filter(cita_id=cita.id).first()
            if diagnostico is None:
                continue
            historial.append({
                'cita': cita,
                'diagnostico': diagnostico,
                'peso': Peso.objects.filter(diagnostico_id=diagnostico.id).first(),
                'medida': Medida.objects.filter(diagnostico_id=diagnostico.id).first(),
                'quiropractica': Quiropractica.objects.filter(diagnostico_id=diagnostico.id).first(),
                'fisioterapia': Fisioterapia.objects.filter(diagnostico_id=diagnostico.id).first(),
            })
        context['historial'] = historial
        return context



class HistorialListView(TemplateView):
    template_name = 'historial/list.html'


class HistorialListJson(BaseDatatableView):
    model = None
    columns = ['id', 'fecha', 'cita.paciente', 'usuario.username']
    order_columns = ['id', 'fecha', 'cita.paciente', 'usuario.username']

    def get_initial_queryset(self):
        query = Diagnostico.objects.order_by('cita__paciente', 'cita__paciente_id')\
            .distinct('cita__paciente', 'cita__paciente_id')

        data = Diagnostico.objects.all().values_list('cita__paciente', flat=True).distinct()
        #query = query.filter(cita__paciente=data)[100]
        return Diagnostico.objects.all()
