from django.contrib.auth.models import User
from django.db import models
from django_extensions.db.models import TimeStampedModel


class Paciente(TimeStampedModel):
    Genero = (
        (1, 'Hombre'),
        (2, 'Mujer')
    )

    Estado = (
        (1, 'Activo'),
        (2, 'Inactivo')
    )

    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    apellido = models.CharField(max_length=100, verbose_name='Apellido')
    direccion = models.CharField(max_length=200, verbose_name='Dirección')
    telefono = models.CharField(max_length=100, verbose_name='Telefono')
    edad = models.CharField(max_length=100, verbose_name='Edad', null=True, blank=True)
    dpi = models.CharField(max_length=100, verbose_name='DPI', null=True, blank=True)
    genero = models.SmallIntegerField(choices=Genero, verbose_name='Género')
    estado = models.SmallIntegerField(choices=Estado, default=1, verbose_name='Estado')

    class Meta:
        app_label = 'clinica'
        verbose_name = 'paciente'
        verbose_name_plural = 'pacientes'

    def __str__(self):
        return '%s %s ' % (self.nombre, self.apellido)


class Doctor(TimeStampedModel):
    Estado = (
        (1, 'Activo'),
        (2, 'Inactivo')
    )

    nombre = models.CharField(max_length=100, verbose_name='Nombre(s)')
    apellido = models.CharField(max_length=100, verbose_name='Apellido(s)')
    telefono = models.CharField(max_length=100, verbose_name='Teléfono')
    estado = models.SmallIntegerField(choices=Estado, default=1, verbose_name='Estado')

    class Meta:
        app_label = 'clinica'
        verbose_name = 'doctor'
        verbose_name_plural = 'doctor'

    def __str__(self):
        return '%s %s ' % (self.nombre, self.apellido)


class Categoria(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')

    class Meta:
        app_label = 'clinica'
        verbose_name = 'categoria'
        verbose_name_plural = 'categorias'

    def __str__(self):
        return self.nombre


class Cita(TimeStampedModel):
    fecha = models.DateField(verbose_name='Fecha')
    hora = models.TimeField(verbose_name='Hora')
    categoria = models.ForeignKey(Categoria, verbose_name='Categoria', related_name='categoria_cita',
                                  on_delete=models.PROTECT)
    doctor = models.ForeignKey(Doctor, verbose_name='Doctor', related_name='doctor_cita', on_delete=models.PROTECT)
    paciente = models.ForeignKey(Paciente, verbose_name='Paciente', related_name='paciente_cita',
                                 on_delete=models.PROTECT)

    class Meta:
        app_label = 'clinica'
        verbose_name = 'cita'
        verbose_name_plural = 'citas'

    def __str__(self):
        return str(self.fecha)


class Diagnostico(models.Model):
    diagnostico = models.TextField(max_length=2000, verbose_name='Diagnostico')
    reseta = models.TextField(max_length=2000, verbose_name='Reseta')
    menu = models.TextField(max_length=2000, verbose_name='Menu', null=True, blank=True)
    fecha = models.DateField(verbose_name='Fecha')

    usuario = models.ForeignKey(User, verbose_name='Usuario', related_name='usuario_diagnostico',
                                on_delete=models.PROTECT)
    cita = models.ForeignKey(Cita, verbose_name='Cita', related_name='cita_diagnostico', on_delete=models.PROTECT)

    class Meta:
        app_label = 'clinica'
        verbose_name = 'diagnostico'
        verbose_name_plural = 'diagnosticos'


class Peso(models.Model):
    peso = models.PositiveIntegerField(verbose_name='Peso', null=True)
    grasa = models.PositiveIntegerField(verbose_name='Grasa', null=True)
    agua = models.PositiveIntegerField(verbose_name='Agua', null=True)
    masa_muscular = models.PositiveIntegerField(verbose_name='Masa Muscular', null=True)
    complexion_fisica = models.PositiveIntegerField(verbose_name='Complexion Fisica', null=True)
    metabolismo_basal = models.PositiveIntegerField(verbose_name='Metabolismo Basal', null=True)
    edad_metabolica = models.PositiveIntegerField(verbose_name='Edad Metabolica', null=True)
    masa_osea = models.PositiveIntegerField(verbose_name='Masa Osea', null=True)

    diagnostico = models.ForeignKey(Diagnostico, verbose_name='Diagnostico', related_name='diagnostico_peso',
                                    on_delete=models.PROTECT)

    class Meta:
        app_label = 'clinica'
        verbose_name = 'peso'
        verbose_name_plural = 'peso'

    def __str__(self):
        return self.peso


class Medida(models.Model):
    abdomen_superior = models.PositiveIntegerField(verbose_name='Abdomen Superior', null=True)
    cintura = models.PositiveIntegerField(verbose_name='Cintura', null=True)
    abdomen_inferior = models.PositiveIntegerField(verbose_name='Abdomen Inferior', null=True)
    circunferencia_brazo = models.PositiveIntegerField(verbose_name='Circunferencia Brazo', null=True)
    circunferencia_muslo = models.PositiveIntegerField(verbose_name='Circunferencia Muslo', null=True)

    diagnostico = models.ForeignKey(Diagnostico, verbose_name='Diagnostico', related_name='diagnostico_medida',
                                    on_delete=models.PROTECT)

    class Meta:
        app_label = 'clinica'
        verbose_name = 'medida'
        verbose_name_plural = 'medida'

    def __str__(self):
        return self.abdomen_superior


class Quiropractica(models.Model):
    precio_q = models.DecimalField(max_digits=18, decimal_places=4, verbose_name='Precio')
    diagnostico = models.ForeignKey(Diagnostico, verbose_name='Diagnostico', related_name='diagnostico_quiropractica',
                                    on_delete=models.PROTECT)

    class Meta:
        app_label = 'clinica'
        verbose_name = 'quiropractica'
        verbose_name_plural = 'quiropractica'

    def __str__(self):
        return self.precio


class Fisioterapia(models.Model):
    precio_f = models.DecimalField(max_digits=18, decimal_places=4, verbose_name='Precio')
    diagnostico = models.ForeignKey(Diagnostico, verbose_name='Diagnostico', related_name='diagnostico_fisioterapia',
                                    on_delete=models.PROTECT)

    class Meta:
        app_label = 'clinica'
        verbose_name = 'fisioterapia'
        verbose_name_plural = 'fisioterapia'

    def __str__(self):
        return self.precio


class Historial(TimeStampedModel):
    diagnostico = models.ForeignKey(Diagnostico, verbose_name='Diagnostico', related_name='diagnostico_historial',
                                    on_delete=models.PROTECT)
    usuario = models.ForeignKey(User, verbose_name='Usuario', related_name='usuario_historial',
                                on_delete=models.PROTECT)
    cita = models.ForeignKey(Cita, verbose_name='Cita', related_name='cita_historial', on_delete=models.PROTECT,
                             null=True, blank=True)

    class Meta:
        app_label = 'clinica'
        verbose_name = 'historial'
        verbose_name_plural = 'historial'
